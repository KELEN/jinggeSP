const formatTime = time => {
  let date = new Date(time);
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const getUrlParam = (url, name) => {
  let reg = new RegExp("[\?|&]" + name + "=([^&]*)(&|$)"); // 构造一个含有目标参数的正则表达式对象
  let r = url.match(reg);  // 匹配目标参数
  if (r != null) return unescape(r[1]); return null; // 返回参数值
}

module.exports = {
  formatTime: formatTime,
  getUrlParam: getUrlParam
}