// const host =  'http://192.168.3.151:8080' 
const host =  'https://wpa.gigaiot.com'

const error = {
  'serviceError': '网络异常，请稍后再试',
  'networkError': '无效的网络'
}

const request = {
  post: function(url, data) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: host + url,
        data: data,
        method: 'POST',
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success (res) {
          resolve(res);
        },
        fail (err) {
          wx.hideLoading();
          wx.showToast({
            title: error.serviceError,
            icon: 'none'
          })
          reject();
        }
      })
    })
  },
  get: function(url, query) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: host + url,
        data: query,
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        method: 'GET',
        success (res) {
          resolve(res);
        },
        fail(err) {
          wx.hideLoading();
          wx.showToast({
            title: error.serviceError,
            icon: 'none'
          })
          reject();
        }
      })
    })
  },
  postWx: function(url, data) {
    return new Promise((resolve, reject, config) => {
      wx.request(Object.assign({
        url: url,
        data: data,
        method: 'POST',
        success: resolve,
        fail: reject
      }, config));
    })
  },
  getWx: function(url, query) {
    return new Promise((resolve, reject) => {
      wx.request({
        url: url,
        data: query,
        method: 'GET',
        success: resolve,
        fail: reject
      })
    })
  },
  error: error
}

module.exports = request;