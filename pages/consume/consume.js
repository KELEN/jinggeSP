const app = getApp()
const request = require('../../utils/request.js')
const util = require('../../utils/util.js')
Page({
  data: {
    userInfo: {},
    selectItem: 0,
    showActionDialog: false,
    deviceId: null,
    balance: 0,
    payMoney: 0,  // 支付金额，
    payChannel: 2,  // 支付渠道
    fromOuterQRcode: false,  // 从外面的二维码进来
    selectList: [{
      id: 1,
      size: 300,
      price: 30
    }, {
      id: 2,
      size: 500,
      price: 50
    }, {
      id: 3,
      size: 1000,
      price: 100
    }]
  },
  back() {
    if (this.data.fromOuterQRcode) {
      wx.redirectTo({
        url: '/pages/index/index',
      })
    } else {
      wx.navigateBack({})
    }
  },
  // 渠道改变
  channelChange(e) {
    this.setData({
      payChannel: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUserInfo();
    let deviceId = null;
    if (options.deviceId) {
      deviceId = options.deviceId;
    } else if (options.q){
      deviceId = util.getUrlParam(decodeURIComponent(options.q), 'deviceId')
    }
    let fromXcx = options.fromXcx;  // 是否小程序扫码进来的
    if (!deviceId) {
      // 改设备不可用
      return;
    } 
    this.setData({
      deviceId: deviceId,
      fromOuterQRcode: !fromXcx
    })
    // wx.getSetting({
    //   success(res) {
    //     if (!res.authSetting['scope.userInfo']) {
    //       wx.navigateTo({
    //         url: '/pages/auth/auth',
    //         success: function (res) { },
    //         fail: function (res) { },
    //         complete: function (res) { },
    //       })
    //     }
    //   }
    // })
  },
  getUserInfo() {
    wx.getUserInfo({
      success: res => {
        app.globalData.userInfo = res.userInfo
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (app.globalData.sid) {
      request.get(`/getBalance?sid=${app.globalData.sid}`)
        .then(res => {
          let data = res.data;
          if (data && !data.error) {
            this.setData({
              balance: (data.balance / 100).toFixed(2)
            })
          }
        })
    } else {
      wx.showLoading({
        title: '加载中',
      })
      wx.login({
        success: res => {
          request.post('/receiveCode', {
            code: res.code
          }).then(res => {
            wx.hideLoading();
            if (!res.code) {
              app.globalData.sid = res.data.sid;
              request.get(`/getBalance?sid=${app.globalData.sid}`)
                .then(res => {
                  let data = res.data;
                  if (data && !data.error) {
                    this.setData({
                      balance: (data.balance / 100).toFixed(2)
                    })
                  }
                })
            }
          }, err => {
            console.log(err);
          })
        }
      });
    }
  },
  deviceOnlineCheck (deviceId) {
    wx.showLoading({
      title: '检查设备状态',
    })
    return request.post(`/onlineCheck?sid=${app.globalData.sid}`, {
      terminalId: deviceId
    })
  },
  moneySelect(ev) {
    let selectItem = ev.currentTarget;
    wx.showToast({
      title: request.error.serviceError,
      icon: 'none'
    })
    this.deviceOnlineCheck(this.data.deviceId).then(res => {
      wx.hideLoading();
      if (res.data.online) {
        this.setData({
          selectItem: selectItem.dataset.item.id,
          payMoney: selectItem.dataset.item.price,
          showActionDialog: true
        })
      } else {
        wx.showToast({
          title: '设备不在线',
          icon: 'none'
        });
      }
    }, err => {
      if (err) {
        wx.showToast({
          title: request.error.serviceError,
          icon: 'none'
        })
      }
    })
  },
  // 支付
  payTap() {
    wx.showLoading({
      title: '支付中...',
    })
    if (this.data.payChannel == 2) {
      request.post(`/wxPay?sid=${app.globalData.sid}`, {
        totalFee: this.data.payMoney,
        terminalId: this.data.deviceId
      }).then(res => {
        wx.hideLoading();
        console.log(res);
        if (!res.code) {
          let data = res.data;
          wx.requestPayment({
            'timeStamp': data.timeStamp,
            'nonceStr': data.nonceStr,
            'package': data.package,
            'signType': 'MD5',
            'paySign': data.paySign,
            'success': res => {
              this.setData({
                showActionDialog: false
              })
              wx.showToast({
                title: '支付成功',
              });
              wx.navigateTo({
                url: '/pages/consume/consumeSuccess',
              })
            },
            'fail': function (res) {
              
            },
            'complete': function(res) {
              
            }
          })
        }
      })
    } else {
      request.post(`/balancePay?sid=${app.globalData.sid}`, {
        totalFee: this.data.payMoney,
        terminalId: this.data.deviceId
      }).then(res => {
        wx.hideLoading();
        if (res.data.success) {
          this.setData({
            showActionDialog: false
          })
          wx.showToast({
            title: '支付成功',
          });
          wx.navigateTo({
            url: '/pages/consume/consumeSuccess',
          })
        } else {
          if (res.data.error == 5) {
            wx.showToast({
              title: '余额不足',
              icon: 'none'
            })
          }
        }
      }, err => {
        wx.hideLoading();
      })
    }
  }
})