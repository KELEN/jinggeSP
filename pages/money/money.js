const request = require('../../utils/request.js')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    balance: 0,
    activeItem: 1,    // 选中的金额
    rechargeMoney: [1, 5, 10, 30, 50, 100]
  },
  
  onShow () {
    this.getMoney()
  },

  getMoney () {
    wx.showLoading({
      title: '加载中',
    })
    request.get(`/getBalance?sid=${app.globalData.sid}`)
      .then(res => {
        wx.hideLoading();
        let data = res.data;
        if (data && !data.error) {
          this.setData({
            balance: (data.balance / 100).toFixed(2)
          })
        } else {
          wx.showToast({
            title: request.error.serviceError,
            icon: 'none'
          })
        }
      }, err => {
        wx.hideLoading();
      })
  },

  moneySelect (e) {
    var target = e.currentTarget;
    var money = target.dataset.num;
    this.setData({
      activeItem: money
    })
  },

  chargeBtnHandle () {
    let _this = this;
    wx.showLoading({
      title: '正在加载...'
    });
    if (!app.globalData.sid) {
      // wx.showToast({
      //   title: '登录超时，请重新登录',
      // })
      wx.showNavigationBarLoading()
    } else {
      request.post(`/recharge?sid=${app.globalData.sid}`, {
        totalFee: this.data.activeItem * 100
      }).then(res => {
        wx.hideLoading();
        if (!res.code) {
          let data = res.data;
          wx.requestPayment({
            'timeStamp': data.timeStamp,
            'nonceStr': data.nonceStr,
            'package': data.package,
            'signType': 'MD5',
            'paySign': data.paySign,
            'success' (res) {
              _this.getMoney();
            },
            'fail' (res) {
              console.log(res);
            }
          })
        }
      })
    }
  },

  back () {
    wx.navigateBack({})
  }
})