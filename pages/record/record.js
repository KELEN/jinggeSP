const app = getApp()
const request = require('../../utils/request.js')
const util = require('../../utils/util.js')
Page({

  data: {
    tabitemConsume: {},
    tabitemRecharge: {},
    activeTabId: null,
    indicatorWidth: 0,
    indicatorOffset: -100,
    rechargeList: [],
    consumeList: [],
    rechargeListPage: {
      total: 0,
      curr: 1
    },
    consumeListPage: {
      total: 0,
      curr: 1
    }
  },

  onReady () {
    var query = wx.createSelectorQuery().in(this),
      _this = this;

    _this.animation = wx.createAnimation()

    query.select('#tabitemConsume').boundingClientRect(function (rect) {
      _this.setData({
        tabitemConsume: rect
      });
      _this.setActiveTab('tabitemConsume');
    })

    query.select('#tabitemRecharge').boundingClientRect(function (rect) {
      _this.setData({
        tabitemRecharge: rect
      });
    })

    query.exec();

    this.loadData();
  },

  loadData () {
    this.loadRechargeList(this.data.consumeListPage.curr)
    this.loadConsumeList(this.data.consumeListPage.curr);
  },

  loadRechargeList(page) {
    request.post(`/getRechargeLogList?sid=${app.globalData.sid}`, {
      page: page, rows: 10
    }).then(res => {
      let { data } = res;
      if (data && !data.error) {
        let arr = this.data.rechargeList.concat(data.data);
        arr.forEach(item => {
          item.cate = 1;
          item.time = util.formatTime(item.time);
        })
        this.setData({
          rechargeList: arr
        })
      }
    })
  },

  loadConsumeList (page) {
    wx.showLoading({
      title: '加载中...',
    })
    request.post(`/getConsumeLogList?sid=${app.globalData.sid}`, {
      page: page, rows: 10
    }).then(res => {
      wx.hideLoading();
      let { data } = res;
      if (data && !data.error) {
        data.data.forEach(function(item) {
          item.cate = 2;
          item.time = util.formatTime(item.time);
        })
        let arr = this.data.consumeList.concat(data.data);
        this.setData({
          consumeList: arr,
          'consumeListPage.total': data.totalPages,
          'consumeListPage.curr': data.currPage
        })
      }
    })
  },

  tabChange (e) {
    var id = e.detail.currentItemId;
    this.setActiveTab(id);
  },

  tabclick (e) {
    var id = e.target.id;
    this.setActiveTab(id);
  },

  setActiveTab(id) {
    var rect = this.data[id];
    if (rect) {
      this.animation.width(rect.width).translate(rect.left, 0);
      this.setData({
        activeTabId: id,
        indicatorAnim: this.animation.step().export()
      })
    }
  },

  scroll () {

  },

  rechargeListToBtom () {
    let { curr, total } = this.data.rechargeListPage;
    if (curr < total) {
      this.loadRechargeList(++curr);
    }
  },

  consumeListToBtom () {
    let { curr, total } = this.data.consumeListPage;
    if (curr < total) {
      this.loadConsumeList(++curr);
    }
  },

  back() {
    wx.navigateBack({})
  }
})