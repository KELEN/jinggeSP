Component({
  // 组件的属性列表
  properties: {
    record: {
      type: Object,
      value: {
        cate: '消费',    // 记录类型 1 充值 2 消费
        title: '',
        money: 0.0,
        num: '',
        date: '',
        finished: true,
        channel: 1,    // 渠道
        before: 0,
        after: 0 
      },
      observer (newVal, oldVal, changedPath) {
        if (newVal.cate === 2) {
          this.setData({
            finishState: newVal.finished ? '已消费': '未找零'
          })
        } else {
          this.setData({
            finishState: newVal.finished ? '已充值' : '充值失败'
          })
        }
      }
    }
  },

  data: {
    finishState: null
  }
})
