//index.js
//获取应用实例
const app = getApp()
const request = require('../../utils/request.js')
const util = require('../../utils/util.js')
Page({
  onShow() {
    this.getUserInfo();
    this.getBalance();
  },
  data: {
    userId: '',
    userInfo: {},
    hasUserInfo: false,
    balance: 0,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    opList: [
      {
        iconBg: '#ffce23',
        iconCls: 'iconfont icon-trade',
        page: 'trade',
        name: '交易记录',
        url: '../record/record'
      }/*,
      {
        iconBg: '#f50057',
        iconCls: 'iconfont icon-refund',
        page: 'refund',
        name: '退款申请',
        url: '../refund/refund'
      },
      {
        iconBg: '#2a76cd',
        iconCls: 'iconfont icon-suggest',
        page: 'suggest',
        name: '意见反馈'
      } */
    ]
  },
  // 获取余额
  getBalance() {
    if (app.globalData.sid) {
      request.get(`/getBalance?sid=${app.globalData.sid}`)
        .then(res => {
          let data = res.data;
          if (data && !data.error) {
            this.setData({
              balance: (data.balance / 100).toFixed(2)
            })
          }
        })
    } else {
      wx.showLoading({
        title: '加载中...',
        mask: true
      })
      wx.login({
        success: res => {
          request.post('/receiveCode', {
            code: res.code
          }).then(res => {
            wx.hideLoading();
            console.log(res)
            if (res && !res.code) {              
              app.globalData.sid = res.data.sid;
              request.get(`/getBalance?sid=${app.globalData.sid}`)
                .then(res => {
                  let data = res.data;
                  if (data && !data.error) {
                    this.setData({
                      balance: (data.balance / 100).toFixed(2)
                    })
                  }
                })
            } else {
              wx.showToast({
                icon: 'none',
                title: request.error.serviceError,
              })
            }
          }, err => {
            if (err.errMsg) {
              wx.showToast({
                icon: 'none',
                title: request.error.serviceError,
              })
            }
          })
        }
      })
    }
  },
  getUserInfo() {
    wx.getUserInfo({
      success: res => {
        app.globalData.userInfo = res.userInfo
        this.setData({
          userInfo: res.userInfo
        })
      },
      fail: res => {}
    })
  },
  // 扫描二维码
  scanQrcode() {
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        let url = res.result;
        let deviceId = util.getUrlParam(url, 'deviceId');
        if (deviceId) {
          wx.navigateTo({
            url: `/pages/consume/consume?deviceId=${ deviceId }&fromXcx=1`,
          })
        } else {
          wx.showToast({
            title: '无效的设备',
            icon: 'none'
          })
        }
      }
    })
  },
  onLoad () {
    // 下一次进来再提示授权
    wx.getSetting({
      success (res) { 
        console.log(res.authSetting['scope.userInfo'], app.globalData.rejectAuth);
        if (!res.authSetting['scope.userInfo'] && !app.globalData.rejectAuth) {
          wx.navigateTo({
            url: '/pages/auth/auth'
          })
        }
      }
    })
  }
})
