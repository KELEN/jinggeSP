const app = getApp();

Page({

  data: {

  },

  getUserInfo(e) {
    app.globalData.userInfo = e.detail.userInfo;
    if (!e.detail.rawData) {
      app.globalData.rejectAuth = true;
    } 
    wx.navigateBack({
      delta: 1
    })
  }
})