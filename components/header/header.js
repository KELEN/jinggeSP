// components/header/header.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    height: '0rpx'
  },

  /**
   * 组件的方法列表
   */
  methods: {

  },
  ready () {
    let _this = this;
    wx.getSystemInfo({
      success: function(res) {
        let sbh = res.statusBarHeight;
        console.log(sbh);
        _this.setData({
          height: sbh + 150 + 'rpx'
        })
      }
    })
  }
})
