// components/actionDialog/actionDialog.js
Component({
  /**
   * 组件的属性列表
   */
  externalClasses: ['action-container'],
  properties: {
    height: {
      type: String,
      value: '300rpx'
    },
    show: {
      type: Boolean,
      value: false,
      observer (newVal, oldVal, changedPath) {
        let opacity = newVal ? 1 : 0;
        this.fadeAnim.opacity(opacity).step();
        this.setData({
          showDialog: newVal
        })
        setTimeout(() => {
          this.setData({
            fadeIn: this.fadeAnim.export()
          })
          this.slideContent(newVal);
        }, 100);
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    showDialog: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    overlayTap() {
      this.setData({
        showDialog: false,
        show: false
      })
    },
    slideContent(show) {
      let yVal = show ? 0: '100%';
      this.slideAnim.translate3d(0, yVal, 0).step();
      this.setData({
        slideup: this.slideAnim.export()
      })
    },
    // stoppropagation
    contentTap () {}
  },

  ready () {
    this.fadeAnim = wx.createAnimation({
      duration: 200,
      timingFunction: 'ease'
    });
    this.slideAnim = wx.createAnimation({
      duration: 200,
      timingFunction: 'ease'
    })
  }
})
