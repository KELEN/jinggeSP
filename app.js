//app.js

const request = require('./utils/request.js')
const config = require('./config.js')

App({
  onLaunch: function (op) {
    this.checkNetwork();
    // 展示本地存储能力
    // var logs = wx.getStorageSync('logs') || []
    // logs.unshift(Date.now())
    // wx.setStorageSync('logs', logs)
  },
  checkNetwork () {
    wx.getNetworkType({
      success: function(res) {
        let nt = res.networkType;
        if (nt == 'none') {
          wx.showToast({
            title: '无网络，请稍后再试',
            icon: 'none',
            duration: 3000
          })
        }
      },
    })

    wx.onNetworkStatusChange((res) => {
      if (!res.isConnected) {
        wx.showToast({
            title: '无网络，请稍后再试',
            icon: 'none',
            duration: 3000
          })
      }
    })
  },
  globalData: {
    userInfo: null,
    sid: null,
    rejectAuth: false
  }
})